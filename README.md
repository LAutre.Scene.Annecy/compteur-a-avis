# Compteur à avis

Compteur manuels d'avis. Ce petit programme permet de créer sur une même page plusieurs compteurs en y choisissant les titres et en allant en négatif ou positif.

Il permet également de sauvegarder ses résultats dans un cookie (durant 3 jours) et d'exporter sur le serveur, dans le même dossier, les résultats dans un format CSV (séparateur ";")


## Format
Version faite principalement pour mobile.


## Licence

Licence CC by SA - L'Autre Scène 2024


## Contributions

Sentez-vous libre de contribuer comme bon vous semble. Ce développement fut rapide, il mérite certainement des améliorations !


## Questions

Pour toutes questions, contactez Casa sur contact@lautre-scene.fr
