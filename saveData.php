<?php

$post = file_get_contents('php://input');


//error handler function
function customError($errno, $errstr) {
  echo "<b>Une erreur s'est produite, copier-coller le texte ci-dessous et l'envoyer à contact@lautre-scene.fr:</b><br/>".$post;
}

//set error handler
set_error_handler("customError");

$date = date("YmdHis");
header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="FDL'.$date.'.csv"');
$user_CSV[0] = array('Idée', 'Nombre');
$counters = explode("#", $post);
$i = 1;
foreach ($counters as $counter) {
    $data = explode("|", $counter);
    $user_CSV[$i] = array($data[0], $data[1]);
    $i++;
}

$fp = fopen('FDL'.$date.'.csv', 'wb');
foreach ($user_CSV as $line) {
    fputcsv($fp, $line, ';');
}
fclose($fp);

echo "OK";
?>